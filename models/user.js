const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

// Schema
const userSchema = new Schema({
    username: {type: String, required: true},
    creationDate: {type: Date, default: Date.now}    
});

// Model export
module.exports = mongoose.model('User', userSchema);