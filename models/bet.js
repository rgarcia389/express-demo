const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

// Schema
const betSchema = new Schema({
    user: {type: String, required: true},
    matchday: {type: Number, min: 1, max: 38, required: true},
    result: {
        home: {type: Number, min: 0, required: true},
        away: {type: Number, min: 0, required: true}
    },  
    dateCreation: {type: Date, default: Date.now}
});

// Virtual for whole result in one string
betSchema.virtual('resultString').get(() => {
    return `${this.result.home} - ${this.result.away}`;
})

// Model export
module.exports = mongoose.model('Bet', betSchema);