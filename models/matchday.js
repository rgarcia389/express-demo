const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

// Schema
const matchdaySchema = new Schema({
    home: {type: String, required: true},
    away: {type: String, required: true},
    number: {type: Number, min: 1, max: 38, required: true},
    result: {
        home: {type: Number, min: 0},
        away: {type: Number, min: 0}
    }
});

// Virtual for whole result in one string
matchdaySchema.virtual('resultString').get(() => {
    return `${this.result.home} - ${this.result.away}`;
})

// Model export
module.exports = mongoose.model('Matchday', matchdaySchema);
