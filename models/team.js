const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

// Schema
const teamSchema = new Schema({
    name: {type: String, required: true},
    shortName: {type: String, required: true},
    region: String,
    bestPlayer: String
})

// Model export
module.exports = mongoose.model('Team', teamSchema);