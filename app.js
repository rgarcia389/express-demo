// Imports
const express       = require('express');
const helmet        = require('helmet');
const mongoose      = require('mongoose');
const morgan        = require('morgan');
const cookieParser  = require('cookie-parser');

// Routes imports
const betRoute      = require('./routes/bets');
const userRoute     = require('./routes/users');
const matchdayRoute = require('./routes/matchdays');
const teamRoute     = require('./routes/teams');

// Config imports
const dbConfig = require('./config/dbConfig');

// App init
const app = express();

// DB init
mongoose.connect(dbConfig.url);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// Middleware 3rd Party
app.use(helmet());
app.use(express.json());
app.use(morgan('dev'));
app.use(cookieParser());

// Routes
app.use('/api/convocatoria/bets',       betRoute);
app.use('/api/convocatoria/users',      userRoute);
app.use('/api/convocatoria/matchdays',  matchdayRoute);
app.use('/api/convocatoria/teams',      teamRoute);

// Listen
app.listen(3000, () => console.log('Listening on port 3000...'));