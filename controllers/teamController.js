const Team = require('../models/team');

// Displays list of all teams
exports.team_list = (req, res) => {
    Team.find((err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        res.send(docs);
    });
};

// Displays details for an specific team
exports.team_details = (req, res) => {
    Team.find({_id: req.params.id}, (err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        if(docs.length === 0) res.status(404).send('Team not found');
        else res.send(docs[0]);
    });
};

// Handles team creation on POST
exports.team_create_post = (req, res) => {
    const toStore = {
        name : req.body.name,
        shortName: req.body.shortName
    }

    if(req.body.region) toStore.region = req.body.region;
    if(req.body.bestPlayer) toStore.bestPlayer = req.body.bestPlayer;

    Team.create(toStore, (err, doc) => {
        if(err) {
            res.status(500).send(`${err}`);
            return;
        }
        res.send(doc);
    });
};