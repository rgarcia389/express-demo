const Matchday  = require('../models/matchday');
const Bet       = require('../models/bet');
const waterfall = require("async/waterfall");
const series = require("async/series");

// Display list of all matchdays
exports.matchday_list = (req, res) => {
    Matchday.find((err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        res.send(docs);
    });
};

// Displays an specific matchday details - By Matchday Number
exports.matchday_details_by_number = (req, res) => {
    Matchday.find({number: req.params.number}, (err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        if(docs.length === 0) res.status(404).send('Matchday not found');
        else res.send(docs[0]);
    });
};

// Displays all winner bets for a given Matchday numbers using Async's waterfall
exports.matchday_winners_waterfall = (req, res) => {

    waterfall([
        (callback) => {
            Matchday.find({number: req.params.number}, (err, docs) => {
                if(err != null){
                    //res.status(500).send(`Matchday: ${err}`);
                    callback(err);
                    return;
                }
                if(docs.length === 0) {
                    //res.status(404).send('Matchday not found');
                    callback(new Error('Matchday not found'));
                    return;
                }
                const winnerResult = docs[0].result;
                callback(null, winnerResult); 
            });
        }, 
        (winnerResult, callback) => { 
            Bet.find({'result.home' : winnerResult.home, 'result.away' : winnerResult.away}, (err, docs) => {
                if(err != null){
                    //res.status(500).send(`Bet: ${err}`);
                    callback(err);
                    return;
                }
                if(docs.length === 0) {
                    //res.status(404).send('No winner bets found');
                    callback(new Error('No winner bets found'));
                    return;
                }
                callback(null, docs); 
            });
        }
        ], (err, result) => {
            if(err != null){
                res.status(500).send(err);
                return;
            }

            res.send(result);
      }
    );
};

exports.matchday_create_post = (req, res) => {
    const toStore = {
        home: req.body.home,
        away: req.body.away,
        number: req.body.number,
        result: null
    };

    if(req.body.result) {

        toStore.result = {
            home: req.body.result.home,
            away: req.body.result.away
        };
    }

    Matchday.create(toStore, (err, doc) => {
        if(err) {
            res.status(500).send(`${err}`);
            return;
        }
        res.send(doc);
    });
};

exports.matchday_update_put = (req, res) => {
    series([
        (callback) => {
            Matchday.find({number: req.params.number}, (err, docs) => {

                if(err != null){
                    callback(err);
                    return;
                }
                
                if(docs.length === 0) {
                    callback(new Error('Matchday not found'))
                    return;
                }

                callback(null);
            });
        },
        (callback) => {
            Matchday.update({number: req.params.number}, {result: req.body.result}, (err, doc) => {
                if(err != null){
                    callback(err);
                    return;
                }
    
                callback(null, doc);
            });
        }
    ], (err, result) => {
        if(err) {
            res.status(500).send(`${err}`);
            return;
        }

        res.send(result);
    })
}