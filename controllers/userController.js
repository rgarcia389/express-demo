const User = require('../models/user');

// Display list of all Users
exports.user_list = (req, res) => {
    User.find((err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        res.send(docs);
    });
};

// Display details for a specific User
exports.user_details = (req, res) => {
    User.find({_id: req.params.id}, (err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        if(docs.length === 0) res.status(404).send('User not found');
        else res.send(docs[0]);
    });
};

// Handles User create on POST
exports.user_create_post = (req, res) => {
    const toStore = {
        username : req.body.username
    }

    User.create(toStore, (err, doc) => {
        if(err) {
            res.status(500).send(`${err}`);
            return;
        }
        res.send(doc);
    });
};

// Handles User removal on DELETE
exports.user_remove_delete = (req, res) => {

};