const Bet = require('../models/bet');

// Display list of all Bets
exports.bet_list = (req, res) => {
    Bet.find((err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        res.send(docs);
    });
};

// Display details for a specific Bet
exports.bet_details = (req, res) => {
    Bet.find({_id: req.params.id}, (err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        if(docs.length === 0) res.status(404).send('Bet not found');
        else res.send(docs[0]);
    });
};

// Display list of all specific user Bets
exports.bet_list_by_username = (req, res) => {
    Bet.find({user: req.params.username}, (err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        res.send(docs);
    });
};

// Handle Bet create on POST
exports.bet_create_post = (req, res) => {
    const toStore = {
        user : req.body.user,
        matchday: req.body.matchday,
        result: {
            home: req.body.result.home,
            away: req.body.result.away
        }
    }

    Bet.create(toStore, (err, doc) => {
        if(err) {
            res.status(500).send(`${err}`);
            return;
        }
        res.send(doc);
    });
};

// Handle Bet update on PUT
exports.bet_update_put = (req, res) => {
    //Find the course
    Bet.find({_id: req.params.id}, (err, docs) => {

        if(err != null){
            res.status(500).send(`${err}`);
            return;
        }
        
        if(docs.length === 0) {
            res.status(404).send('Bet not found');
            return;
        }

        Bet.update({_id: req.params.id}, {result: req.body.result}, (err, doc) => {
            if(err != null){
                res.status(500).send(`${err}`);
                return;
            }

            res.send(doc);
        })
    });
};