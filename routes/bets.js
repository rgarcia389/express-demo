const express   = require('express');
const router    = express.Router();

// Require controller
const bet_controller = require('../controllers/betController');

// Bet Routes

// GET Root - Retrieve list of all bets
router.get('/', bet_controller.bet_list);

// GET by id - Retrieve an specific bet
router.get('/:id', bet_controller.bet_details);

// GET by username - Retrieve a list of an specific user bets
router.get('/byuser/:username', bet_controller.bet_list_by_username);

// POST Root - Creates a bet
router.post('/', bet_controller.bet_create_post);

// PUT by id - Updates a bet (only result)
router.put('/:id', bet_controller.bet_update_put);

// Export
module.exports = router;
