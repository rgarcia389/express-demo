const express   = require('express');
const router    = express.Router();

// Require controller
const team_controller = require('../controllers/teamController');

// Team routes

// GET Root - Retrieves all teams list
router.get('/', team_controller.team_list);

// GET by id - Retrieves an specific team
router.get('/:id', team_controller.team_details);

// POST Root - Creates a new team
router.post('/', team_controller.team_create_post);

// Export
module.exports = router;