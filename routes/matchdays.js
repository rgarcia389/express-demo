const express   = require('express');
const router    = express.Router();

// Require controller
const matchday_controller = require('../controllers/matchdayController');

// Matchday routes

// GET Root - Retrieves list of all matchdays
router.get('/', matchday_controller.matchday_list);

// GET by id - Retrieves an specific Matchday user details (by number)
router.get('/:number', matchday_controller.matchday_details_by_number);

// GET winners - Retrieve the list of winner bets given an specific matchday number
router.get('/:number/winners', matchday_controller.matchday_winners_waterfall);

// POST Root - Handles Matchday creation on POST
router.post('/', matchday_controller.matchday_create_post);

// PUT by id - Handles Matchday updates on PUT
router.put('/:number', matchday_controller.matchday_update_put);

// Export
module.exports = router;