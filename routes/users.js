const express   = require('express');
const router    = express.Router();

// Require controller
const user_controller = require('../controllers/userController');

// User routes

// GET Root - Retrieves list of all users
router.get('/', user_controller.user_list);

// GET by id - Retrieves an specific user details
router.get('/:id', user_controller.user_details);

// POST Root - Creates a user
router.post('/', user_controller.user_create_post);

// DELETE Root - Deletes a user
router.delete('/', user_controller.user_remove_delete);

// Export
module.exports = router;